public with sharing class AppEmailQueueable implements Queueable{
    private List<User> recipients;
    private FeedItem question;
    private static String ROOT_QUESTION_URL = 'https://myqa2-developer-edition.na139.force.com/qa/s/question/';

    public AppEmailQueueable(List<User> recipients, FeedItem question) {
        this.recipients = recipients;
        this.question = question;
        System.debug('Constructor called:');
        System.debug(this.recipients);
        System.debug(this.question);
    }

    public void execute(QueueableContext context) {
        System.debug('Executing AppEmailQueueable...');
        System.debug(recipients);
        System.debug(question);

        List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
        Id senderId = getSenderId();
        for(User eachRecipient : recipients) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTargetObjectId(eachRecipient.Id);
            email.setTreatTargetObjectAsRecipient(true);
            email.setSubject(buildTitle(question.Title));
            email.setHtmlBody(buildBody(question.Body, question.Id));
            email.setSaveAsActivity(false);
            if(senderId != null) {
                email.setOrgWideEmailAddressId(senderId);
            }
            emailsToSend.add(email);
        }
        Messaging.sendEmail(emailsToSend);
    }

    private Id getSenderId()
    {
        List<OrgWideEmailAddress> owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'cplant1776+system@gmail.com'];
        if ( owea.size() > 0 ) {
            return owea.get(0).Id;
        } else {
            return null;
        }
    }

    private String buildTitle(String questionTitle)
    {
        return 'New Question: ' + questionTitle;
    }

    private String buildBody(String questionBody, Id recordId)
    {
        String result = 'A new question has been posted! See the contents below:' + '<br>';
        result += questionBody;
        result += '<br>' + '<a href="' + ROOT_QUESTION_URL + recordId + '">See if you can answer it</a>';
        return result;
    }
}