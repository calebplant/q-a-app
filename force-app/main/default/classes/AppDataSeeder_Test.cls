@isTest
public class AppDataSeeder_Test {
    
    @isTest(seeAllData=true)
    static void insertedSeedData()
    {
        List<String> topics = new List<String>();
        topics.add('R');
        topics.add('Python');

        AppDataSeeder seeder = new AppDataSeeder();
        seeder.initializeData(topics);
        //seeder.printData();
        seeder.seedNQuestions(50, 'Python');
        seeder.seedNQuestions(50, 'R');
        seeder.seedNAnswersToQuestions(200, 'Python');
        seeder.seedNAnswersToQuestions(200, 'R');
        seeder.seedNRepliesToAnswers(200, 'Python');
        seeder.seedNRepliesToAnswers(200, 'R');

        Integer feedItemListSize = [SELECT COUNT() FROM FeedItem];
        System.assertNotEquals(0, feedItemListSize);
        seeder.deletePosts();
        feedItemListSize = [SELECT COUNT() FROM FeedItem];
        System.assertEquals(0, feedItemListSize);
        // seeder.seedNAnswersToQuestions(200, 'R');

    }
    
}