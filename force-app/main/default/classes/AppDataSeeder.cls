public without sharing class AppDataSeeder {

    private Map<String, List<Seed_Question__c>> questions = new Map<String, List<Seed_Question__c>>();
    private Map<String, List<Seed_Answer__c>> answers = new Map<String, List<Seed_Answer__c>>();

    // List<FeedItem> insertedQuestions = new List<FeedItem>();
    // List<FeedComment> insertedAnswers = new List<FeedComment>();

    Set<Id> insertedQuestionIds = new Set<Id>();
    Set<Id> insertedAnswerIds = new Set<Id>();
    Map<Id, Id> originalPostIdByAnswerId = new Map<Id, Id>();

    private List<User> appUsers = new List<User>();
    private Map<String, Topic> topics = new Map<String, Topic>();

    public void initializeData(List<String> topicList)
    {
        // Questions/Answers
        for(String eachTopic : topicList) {
            questions.put(eachTopic, [SELECT Id, Title__c, Body_Content__c FROM Seed_Question__c
                                        WHERE Topic__c=:eachTopic]);
            answers.put(eachTopic, [SELECT Id, Body_Content__c FROM Seed_Answer__c
                                WHERE Topic__c=:eachTopic]);
        }
        // Users
        Profile appUserProfile = [SELECT Id FROM Profile WHERE Name = 'Quality Assurance User' LIMIT 1];
        appUsers = [SELECT Id FROM User WHERE ProfileId = :appUserProfile.Id];
        // Topics
        List<Topic> foundTopics = [SELECT Id, Name FROM Topic WHERE Name IN :topicList];
        for(Topic eachTopic : foundTopics) {
            topics.put(eachTopic.Name, eachTopic);
        }
    }

    public void seedNQuestions(Integer numOfQuestions, String questionTopic)
    {
        System.debug('START seedNQuestions');
        List<FeedItem> insertedQuestions = new List<FeedItem>();
        if(numOfQuestions > questions.get(questionTopic).size()) {
            throw new ParameterException('numOfQuestions parameter larger than available question pool.');
        }

        List<Seed_Question__c> allQuestions = new List<Seed_Question__c>();
        allQuestions.addAll(questions.get(questionTopic));
        // System.debug('allQuestions: ' + allQuestions);
        List<Seed_Question__c> questionPool = new List<Seed_Question__c>();
        while(questionPool.size() < numOfQuestions) {
            Integer index = Utils.getRandomInteger(allQuestions.size());
            questionPool.add(allQuestions[index]);
            allQuestions.remove(index);
        }

        Id topicId = topics.get(questionTopic).Id;

        for(Seed_Question__c eachQuestion : questionPool) {

            User u = (User)Utils.getRandomElement(appUsers);

            FeedItem newQuestion = new FeedItem();
            newQuestion.Title = eachQuestion.Title__c;
            newQuestion.Body = eachQuestion.Body_Content__c;
            newQuestion.IsRichText = True;
            newQuestion.createdById = u.Id;
            newQuestion.ParentId = topicId;
            newQuestion.Type = 'QuestionPost';
            insertedQuestions.add(newQuestion);
        }

        for(FeedItem eachQuestion : insertedQuestions) {
            // System.debug(eachQuestion);
        }

        Database.insert(insertedQuestions, false);
        for(FeedItem eachQuestion : insertedQuestions) {
            insertedQuestionIds.add(eachQuestion.Id);
        }
    }

    public void seedNAnswersToQuestions(Integer numOfAnswers, String answerTopic)
    {
        System.debug('START seedNAnswersToQuestions');
        List<FeedComment> insertedAnswers = new List<FeedComment>();
        if(numOfAnswers > answers.get(answerTopic).size()) {
            throw new ParameterException('numOfAnswers parameter larger than available answer pool.');
        }

        List<Seed_Answer__c> allAnswers = new List<Seed_Answer__c>();
        allAnswers.addAll(answers.get(answerTopic));
        List<Seed_Answer__c> answerPool = new List<Seed_Answer__c>();
        while(answerPool.size() < numOfAnswers) {
            Integer index = Utils.getRandomInteger(allAnswers.size());
            answerPool.add(allAnswers[index]);
            allAnswers.remove(index);
        }
        List<Id> questionIds = new List<Id>(insertedQuestionIds);

        for(Seed_Answer__c eachAnswer : answerPool) {
            User u = (User)Utils.getRandomElement(appUsers);

            FeedComment newComment = new FeedComment();
            newComment.IsRichText = True;
            newComment.FeedItemId = (Id)Utils.getRandomElement(questionIds);
            newComment.createdById = u.Id;
            newComment.commentBody = eachAnswer.Body_Content__c;
            insertedAnswers.add(newComment);
        }

        for(FeedComment eachAnswer : insertedAnswers) {
            // System.debug(eachAnswer);
        }

        Database.insert(insertedAnswers, false);
        for(FeedComment eachAnswer : insertedAnswers) {
            insertedAnswerIds.add(eachAnswer.Id);
            originalPostIdByAnswerId.put(eachAnswer.Id, eachAnswer.FeedItemId);
        }
    }

    public void seedNRepliesToAnswers(Integer numOfReplies, String replyTopic)
    {
        System.debug('START seedNRepliesToAnswers');
        List<FeedComment> insertedReplies = new List<FeedComment>();
        if(numOfReplies > answers.get(replyTopic).size()) {
            throw new ParameterException('numOfReplies parameter larger than available answer pool.');
        }

        List<Seed_Answer__c> allAnswers = new List<Seed_Answer__c>();
        allAnswers.addAll(answers.get(replyTopic));
        List<Seed_Answer__c> answerPool = new List<Seed_Answer__c>();
        while(answerPool.size() < numOfReplies) {
            Integer index = Utils.getRandomInteger(allAnswers.size());
            answerPool.add(allAnswers[index]);
            allAnswers.remove(index);
        }
        List<Id> answerIds = new List<Id>(insertedAnswerIds);

        for(Seed_Answer__c eachAnswer : answerPool) {
            User u = (User)Utils.getRandomElement(appUsers);
            Id threadParentId = (Id)Utils.getRandomElement(answerIds);

            FeedComment newComment = new FeedComment();
            newComment.IsRichText = True;
            newComment.FeedItemId = originalPostIdByAnswerId.get(threadParentId);
            newComment.ThreadParentId = threadParentId;
            newComment.createdById = u.Id;
            newComment.commentBody = eachAnswer.Body_Content__c;
            insertedReplies.add(newComment);
        }

        for(FeedComment eachReply : insertedReplies) {
            // System.debug(eachReply);
        }

        Database.insert(insertedReplies, false);
    }

    public void deletePosts()
    {
        List<FeedItem> feedItemList = [SELECT Id FROM FeedItem];
        delete feedItemList;
    }

    public void printData()
    {
        System.debug('======== Questions ===========');
        for(List<Seed_Question__c> questionList : questions.values()) {
            for(Seed_Question__c eachQuestion : questionList) {
                System.debug(eachQuestion);
            }
        }
        System.debug('======== Answers ===========');
        for(List<Seed_Answer__c> answerList : answers.values()) {
            for(Seed_Answer__c eachAnswer : answerList) {
                System.debug(eachAnswer);
            }
        }
    }
}