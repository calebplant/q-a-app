@isTest
public with sharing class QASearchController_Test {
    
    private static String QA_USER_1_LAST_NAME = 'Brown';
    private static String QA_USER_2_LAST_NAME = 'Fuller';
    private Static String TOPIC_NAME = 'fruit';

    @TestSetup
    static void makeData(){
        // Post a quesiton as each user
        User user1 = [SELECT Id, Name, Username FROM User WHERE LastName = :QA_USER_1_LAST_NAME LIMIT 1];
        User user2 = [SELECT Id, Name, Username FROM User WHERE LastName = :QA_USER_2_LAST_NAME LIMIT 1];
        Topic newTopic = new Topic(Name=TOPIC_NAME);
        insert newTopic;

            FeedItem newQuestion1 = new FeedItem();
            newQuestion1.Title = 'Are bananas healthy';
            newQuestion1.Body = 'Please see title.';
            newQuestion1.IsRichText = True;
            newQuestion1.createdById = user1.Id;
            newQuestion1.ParentId = newTopic.Id;
            newQuestion1.Type = 'QuestionPost';
            insert newQuestion1;

            FeedItem newQuestion2 = new FeedItem();
            newQuestion2.Title = 'apples good?';
            newQuestion2.Body = '';
            newQuestion2.IsRichText = True;
            newQuestion2.createdById = user2.Id;
            newQuestion2.ParentId = newTopic.Id;
            newQuestion2.Type = 'QuestionPost';
            insert newQuestion2;

            FeedComment newComment = new FeedComment();
            newComment.IsRichText = True;
            newComment.FeedItemId = newQuestion1.Id;
            newComment.createdById = user2.Id;
            newComment.commentBody = 'Grapes';
    }

    @isTest
    static void doesSearchForPostsReturnProper()
    {
        Id questionId = [SELECT Id FROM FeedItem WHERE Type = 'QuestionPost' LIMIT 1].Id;
        List<Id> fixedSearchResult = new List<Id>{questionId};
        Test.setFixedSearchResults(fixedSearchResult);
        Test.startTest();
        List<QASearchController.searchResult> bananaResult = QASearchController.searchForPosts('banana');
        Test.stopTest();

        System.assertEquals(1, bananaResult.size());
    }
}
