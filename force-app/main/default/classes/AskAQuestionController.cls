public with sharing class AskAQuestionController {

    @AuraEnabled
    public static void postQuestion(PostDetails details)
    {
        System.debug('AskAQuestionController :: postQuestion');

        System.debug('details: ' + details);
        String bodyText = Utils.sanitizeRichText(details.body);

        Id userId = UserInfo.getUserId();
        Topic selectedTopic = [SELECT Id FROM Topic WHERE Name=:details.topicName LIMIT 1];

        FeedItem newQuestion = new FeedItem();
        newQuestion.Title = details.title;
        newQuestion.Body = bodyText;
        newQuestion.IsRichText = True;
        newQuestion.ParentId = selectedTopic.Id;
        newQuestion.Type = 'QuestionPost';
        System.debug('New Question: ' + newQuestion);
        insert newQuestion;
        
    }

    public class PostDetails
    {
        @AuraEnabled
        public String topicName {get; set;}
        @AuraEnabled
        public String title {get; set;}
        @AuraEnabled
        public String body {get; set;}
        @AuraEnabled
        public List<AttachmentDetails> attachments {get; set;}
    }

    public class AttachmentDetails
    {
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String documentId {get; set;}
    }
}