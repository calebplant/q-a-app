public with sharing class Utils {

    public static Object getRandomElement(List<Object> objs)
    {
        return objs[getRandomInteger(objs.size() - 1)];
    }

    public static Integer getRandomInteger(Integer max)
    {
        return Integer.valueOf(Math.random() * max);
    }

    public static String sanitizeRichText(String content)
    {
        String result = content;
        result = result.replace('<br>', '<p>&nbsp;</p>');
        return result;
    }
}