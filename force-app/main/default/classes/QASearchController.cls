public without sharing class QASearchController {

    // @AuraEnabled
    // public static List<String> searchForIds(String searchText) {
    //     List<List<SObject>> results = [FIND :searchText IN ALL FIELDS  RETURNING Account(Id), Contact(Id), Lead(Id)];
    //     List<String> ids = new List<String>();
    //     for (List<SObject> sobjs : results) {
    //       for (SObject sobj : sobjs) {
    //         ids.add(sobj.Id);
    //       }
    //     }
    //     System.debug('Ids:');
    //     System.debug(ids);
    //     return ids;
    // }

    @AuraEnabled
    public static List<searchResult> searchForPosts(String searchText) {
        searchTerm search = new searchTerm(searchText);
        if(String.isEmpty(searchText)) { 
            return new List<searchResult>();
        }
        // System.debug(search.getQueryString());

        List<List<SObject>> results = [FIND :search.getQueryString() IN ALL FIELDS  
                                        RETURNING FeedItem(Id, Title, CreatedById, CreatedDate WHERE Type='QuestionPost'),
                                                   FeedComment(Id, FeedItemId, CreatedById, CreatedDate)
                                        LIMIT 2000];
        Map<Id, User> userById = new Map<Id, User>(); 
        Map<Id, FeedItem> FeedItemById = new Map<Id, FeedItem>();        
        if(results[0].size() > 0) {
            userById.putAll(getQuestionUsers(results[0]));
            userById.putAll(getCommentUsers(results[1]));
            System.debug(userById);
        }
        if(results[1].size() > 0) {
            feedItemById = getFeedItemMap(results[1]);
            System.debug(FeedItemById);
        }

        List<searchResult> searchResults = new List<searchResult>();
        for (List<SObject> sobjs : results) {
            for (SObject sobj : sobjs) {
                System.debug(sobj);
                SearchResult searchResult = new searchResult((Id)sobj.get('Id'));
                if(searchResult.objectApiName == 'FeedItem') {
                    searchResult.title = (String)sobj.get('Title');
                } else if(searchResult.objectApiName == 'FeedComment') {
                    searchResult.feedTitle = feedItemById.get((Id)sobj.get('FeedItemId')).Title;
                }
                searchResult.creatorName = userById.get((Id)sobj.get('CreatedById')).Name;
                searchResult.creatorId = (Id)sobj.get('CreatedById');
                searchResult.setFormattedDates((Datetime)sobj.get('CreatedDate'));
                searchResults.add(searchResult);
                System.debug('Added');
            }
        }
        System.debug('Returning:');
        System.debug(searchResults);
        return searchResults;
    }

    private static Map<Id, User> getQuestionUsers(List<FeedItem> feedItems)
    {
        System.debug('START getQuestionUsers');
        List<Id> userIds = new List<Id>();
        for(FeedItem eachItem : feedItems) {
            userIds.add(eachItem.CreatedById);
        }
        return new Map<Id, User>([SELECT Id, Name FROM User WHERE Id IN :userIds]);
    }

    private static Map<Id, User> getCommentUsers(List<FeedComment> comments)
    {
        System.debug('START getCommentUsers');
        List<Id> userIds = new List<Id>();
        for(FeedComment eachComment : comments) {
            userIds.add(eachComment.CreatedById);
        }
        return new Map<Id, User>([SELECT Id, Name FROM User WHERE Id IN :userIds]);
    }

    private static Map<Id, FeedItem> getFeedItemMap(List<FeedComment> comments)
    {
        System.debug('START getFeedItemMap');
        List<Id> feedIds = new List<Id>();
        for(FeedComment eachComment : comments) {
            feedIds.add(eachComment.FeedItemId);
        }
        return new Map<Id, FeedItem>([SELECT Id, Title FROM FeedItem WHERE
                Type = 'QuestionPost' AND Id IN :feedIds]);
    }

    public class searchResult
    {
        @AuraEnabled
        public String recordId;
        @AuraEnabled
        public String objectApiName;
        @AuraEnabled
        public String title;
        @AuraEnabled
        public String creatorName;
        @AuraEnabled
        public Id creatorId;
        @AuraEnabled
        public String feedTitle;
        @AuraEnabled
        public String displayDate;
        @AuraEnabled
        public String createTime;
        
        public searchResult(Id recId)
        {
            recordId = recId;
            objectApiName = '' + recId.getSobjectType();
        }

        public void setFormattedDates(Datetime d)
        {
            createTime = d.format('YYYY-MM-dd');
            displayDate = d.format();
        }
    }

    public class searchTerm
    {
        public String fullTerm;
        public String allTerms = '';

        public searchTerm(String term) {
            fullTerm = '"' + term + '"';
            List<String> terms = term.split(' ');
            for(String eachTerm : terms) {
                allTerms += '"' + eachTerm + '" OR ';
            }
            allTerms = allTerms.substring(0, allTerms.length()-4);
            // System.debug('Constructer Called: ' + allTerms);
        }

        public String getQueryString()
        {
            String result = fullTerm;
            if(!String.isEmpty(allTerms)) {
                result += ' OR ' + allTerms;
            }
            // System.debug('Query String: ' + result);
            // System.debug(fullTerm);
            // System.debug(allTerms);
            return result;
        }
    }
}