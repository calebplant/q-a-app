public with sharing class AppEmailService {
    
    @InvocableMethod(label='Email Q&A Members' description='Send newly posted questions to all QA community members' category= 'Network')
    public static void broadcastNewQuestion(List<FeedItem> questions)
    {
        System.debug('broadcastNewQuestion');
        System.debug(questions);
        List<User> communityMembers = getQAMembers();
        System.debug('Members: ');
        System.debug(communityMembers);

        for(FeedItem eachQuestion : questions) {
            List<User> memberBatch = new List<User>();
            for(User eachMember : communityMembers) {
                if(eachMember.Id != eachQuestion.CreatedById) { // exclude question creator's email
                    if(memberBatch.size() <= 249) {
                        memberBatch.add(eachMember);
                    } else {
                        System.debug('Enqueue email job...');
                        System.enqueueJob(new AppEmailQueueable(memberBatch, eachQuestion));
                        memberBatch = new List<User>();
                    }
                }
            }

            if(memberBatch.size() > 0) {
                System.debug('Enqueue job for any remaining emails...');
                System.enqueueJob(new AppEmailQueueable(memberBatch, eachQuestion));
            }
        }
    }

    private static List<User> getQAMembers()
    {
        System.debug('Getting community members . . .');

        Id communityId = [SELECT Id FROM Network
                 WHERE Name = 'Quality Assurance App'].Id;
        List<NetworkMember> members = [SELECT Id, MemberId FROM NetworkMember
                                    WHERE NetworkId=:communityId];
        List<Id> memberIds = new List<Id>();
        for(NetworkMember eachMember : members) {
            memberIds.add(eachMember.MemberId);
        }

        return [SELECT Id, Email FROM User WHERE Id IN :memberIds];
    }

}