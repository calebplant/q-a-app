({
    handleClick : function(component, event, helper) {
      console.log('handleClick');
      
      var searchText = component.get('v.searchText');
      var action = component.get('c.searchForPosts');
        // var action = component.get('c.searchForIds');
      action.setParams({searchText: searchText});
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
            // var ids = response.getReturnValue();
            console.log('Response:');
            console.log(response.getReturnValue());
            var recordResults = response.getReturnValue();
            
            // console.log(ids);
            sessionStorage.setItem('qaSearch--recordResults', JSON.stringify(recordResults));
            // sessionStorage.setItem('qaSearch--recordResults', JSON.stringify(ids));
            var navEvt = $A.get('e.force:navigateToURL');
            navEvt.setParams({url: '/qa-search-results'});
            navEvt.fire();
        }
      });
      $A.enqueueAction(action);
    },

    handleQueryLookup : function(component, event, helper) {
      console.log('handleQueryLookup');
      console.log(event.getParam('selectedContent'));
      let selectedQuery = event.getParam('selectedContent');
      component.set('v.searchText', selectedQuery);
    },

    handleSearchTermUpdate : function(component, event, helper) {
      // console.log('handleSearchTermUpdate');
      let updatedSearchTerm = event.getParam('searchTerm');
      component.set('v.searchText', updatedSearchTerm);
    }
})