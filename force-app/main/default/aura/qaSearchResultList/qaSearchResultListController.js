({
    init: function(component, event, helper) {
      let items = [
        {"label": "Top Questions", "value": "topQuestions"},
        {"label": "Recent Questions", "value": "recentQuestions"},
        // {"label": "Recent Activity", "value": "recentActivity"},
      ];
      component.set("v.options", items);

      var recordsJson = sessionStorage.getItem('qaSearch--recordResults'); 
      if (!$A.util.isUndefinedOrNull(recordsJson)) {
        var results = JSON.parse(recordsJson);
        component.set('v.results', results);
        component.set('v.sortedResults', results);
        sessionStorage.removeItem('qaSearch--recordResults'); 
      }
    },

    handleChange: function(component, event, helper) {
      var selectedOptionValue = event.getParam("value");
      var results = component.get('v.results');
      var sortedResults = [];
      switch(selectedOptionValue) {
        case 'topQuestions':
          sortedResults = results;
          break;
        case 'recentQuestions':
          sortedResults = helper.sortOnRecentQuestion(results);
          break;
        // case 'recentActivity':
        //   break;
      }
      component.set('v.sortedResults', sortedResults);
    },
})