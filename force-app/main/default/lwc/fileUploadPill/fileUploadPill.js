import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class FileUploadPill extends NavigationMixin(LightningElement) {

    @api fileName;
    @api documentId;

    handleClick() {
        console.log('fileUploadPill :: handleClick');
        
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state:{
                selectedRecordId: this.documentId
            }
        });
    }

    handleRemoveOnly(event) {
        console.log('handleRemoveOnly');
        event.preventDefault();
        this.dispatchEvent(new CustomEvent('removeclick', {detail: this.documentId}));        
    }
}