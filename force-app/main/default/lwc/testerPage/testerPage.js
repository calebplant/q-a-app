import { LightningElement } from 'lwc';

export default class TesterPage extends LightningElement {
    accountId;
    queryId;

    handleAccountLookup (event) {
      this.accountId = event.detail
    }

    handleQueryLookup (event) {
        this.queryId = event.detail;
    }
}