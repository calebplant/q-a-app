import { LightningElement, api } from 'lwc';
import { createRecord, deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import postQuestion from '@salesforce/apex/AskAQuestionController.postQuestion';
 
export default class CustomAskAQuestion extends LightningElement {
 
    @api title;
    @api buttonlabel;

    isModalOpen = false;
    selectedTopic;
    questionTitle;
    questionBody;
    section = '';
    uploadedFiles;

    recordDetails = {};

 
    // @track subject = '';
    // @track desc = '123234345345';

    get topicOptions() {
        return [
            {label: 'Dogs', value: 'Dogs'},
            {label: 'Python', value: 'Python'},
            {label: 'R', value: 'R'},
        ]
    }

    get enabledFormats() {
        return ['bold', 'italic', 'underline', 'strike', 'clean', 'list', 'image', 'link'];
    }

    handleSelectTopic(event) {
        this.selectedTopic = event.detail.value;
    }

    handleClick() {
        console.log('Open modal');
        this.openModal(); 
    }
    
    handleFormInputChange(event){
        this.recordDetails[event.target.name] = event.target.value;
        // console.log(event.target.name + ' now is set to ' + event.target.value);
    }

    handleRichTextUpdate(event){
        this.recordDetails.questionBody = event.target.value;
    }

    handleToggleSection(event) {
        if(this.section == event.detail.openSections) {
            this.section = '';
        } else {
            this.section = event.detail.openSections;
        }
    }

    handleUploadFinish(event) {
        console.log('handleUploadFinish');
        this.uploadedFiles = event.detail.files;
        console.log('Uploaded:');
        console.log(JSON.parse(JSON.stringify(this.uploadedFiles)));
    }

    handleRemoveFile(event) {
        let documentId = event.detail;
        deleteRecord(documentId)
            .then(() => {
                this.removeUploadedFile(documentId);
            })
            .catch(error => {
                new ShowToastEvent({
                    title: 'Error deleting record',
                    message: error.body.message,
                    variant: 'error'
                });
            });
    }

    // Utils
    openModal() {
        this.isModalOpen = true;
    }
    closeModal() {
        this.isModalOpen = false;
    }
    submitDetails() {
        let postDetails = {
            topicName: this.selectedTopic,
            title: this.recordDetails.questionTitle,
            body: this.recordDetails.questionBody,
            attachments: this.uploadedFiles
        }
        console.log('Payload: ');
        console.log(JSON.parse(JSON.stringify(postDetails)));
        postQuestion({details: postDetails})
            .then(result => {
                console.log('Successfully posted.');
            })
            .catch(error => {
                console.log('Error posting!');
                console.log(error);
            });
        this.isModalOpen = false;
    }

    removeUploadedFile(targetId) {
        this.uploadedFiles = this.uploadedFiles.filter(eachFile => {
            return eachFile.documentId != targetId;
        });
    }

    
}