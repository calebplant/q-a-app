import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    createRecord
} from 'lightning/uiRecordApi';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import CASE_OBJECT from '@salesforce/schema/Case';
import SUBJECT_FIELD from '@salesforce/schema/Case.Subject';
import DESC_FIELD from '@salesforce/schema/Case.Description';
import STATUS_FIELD from '@salesforce/schema/Case.Status';
import ORIGIN_FIELD from '@salesforce/schema/Case.Origin';
 
export default class Communityexample extends LightningElement {
 
    @api title;
    @api buttonlabel;
 
    @track subject = '';
    @track desc = '123234345345';
 
    handleChange(event) {
        console.log('two bananas');
        if (event.target.label === 'Case Subject') {
            this.subject = event.target.value;
        }
        if (event.target.label === 'Case Description') {
            this.desc = event.target.value;
        }
    }
    submitCase() {
        console.log('bananas');
        
        const fields = {};
        fields[SUBJECT_FIELD.fieldApiName] = this.subject;
        fields[DESC_FIELD.fieldApiName] = this.desc;
        fields[STATUS_FIELD.fieldApiName] = 'New';
        fields[ORIGIN_FIELD.fieldApiName] = 'Web';
 
        const recordInput = {
            apiName: CASE_OBJECT.objectApiName,
            fields
        };
        createRecord(recordInput)
            .then(theCase => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Case is submitted : ' + theCase.id,
                            variant: 'success',
                        }),
                    );
                })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error creating record',
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }
}