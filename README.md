# Q&A App

## Overview

[Public community](https://myqa2-developer-edition.na139.force.com/qa) built on the customer service template. Users can sign themselves up, post and answer questions, and vote on the best responses. Users can utilize a custom search bar to look for existing questions, and are alerted via email when a new question is posted by another user.

## Function/File Overview

### Email Alerts

When a new question is posted, a flow invokes [AppEmailService](force-app/main/default/classes/AppEmailService.cls) which queues up a series of [AppEmailQueueable](force-app/main/default/classes/AppEmailQueueable.cls) to send emails to the community members. The email body contains the newly posted question and a link to its page.

![Example Email](media/email.png)

### Custom Search

The built-in Global Search for Peer-to-Peer Communities component seems adequate, but I still built a custom search component for the experience. The root component ([qaSearch](force-app/main/default/aura/qaSearch)) contains a search bar and a button to submit the query. It is backed up by the [QASearchController](force-app/main/default/classes/QASearchController.cls).

The search bar displays suggested queries as the user types by checking against a custom Suggested Query object. The bar itself ([lookup](force-app/main/default/lwc/lookup)) is a modified lightning web component based on [this open-source project](https://github.com/jlyon87/lwc-lookup). Its controller is [LookupAuraService](force-app/main/default/classes/LookupAuraService.cls). 

![Custom Search Bar](media/searchBar.png)

When a user submits their query, they are taken to a result page built by [qaSearchResultList](force-app/main/default/aura/qaSearchResultList), which builds a [qaSearchResultItem](force-app/main/default/aura/qaSearchResultItem) for each found post. Each item shows a hyperlinked post title, hyperlinked creator name, and the date it was posted.

![Search Result Page](media/searchResultPage.png)
